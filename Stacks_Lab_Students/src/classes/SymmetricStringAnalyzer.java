package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}
	
	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() {
	
	SLLStack<Character> stack = new SLLStack<Character>(); 
	
	for (int i=0; i < s.length(); i++) { 
		char c = this.s.charAt(i); 
		if (Character.isLetter(c))
		   if (Character.isUpperCase(c))
			  stack.push(c); 
		
			  else if (stack.isEmpty())
				 return false; 
		
			   else {
				 char t = stack.top(); 
				 if (t == Character.toUpperCase(c))
				    stack.pop();  
				 else 
				    return false; 
				}
		
			else 
				return false; 
	} 
	
	if(!stack.isEmpty())
		return false; 
	
	return true; 
}

	
	public String toString() { 
		return s; 
	}

	public String parenthesizedExpression() 
	throws StringIsNotSymmetricException 
	{
		String s = this.toString();
		String s2 = "";
		if(this.isValidContent()) {
			for (int i = 0; i < s.length(); i++) {
				if(Character.isUpperCase(s.charAt(i))) s2 += " <" + s.charAt(i);
				else s2 += " " + s.charAt(i) + ">";
			}
			return s2;
		}
		else
			throw new StringIsNotSymmetricException();	}

}
